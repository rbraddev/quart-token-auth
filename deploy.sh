#!/bin/bash
set -e
mkdir -p frontend backend
docker-compose pull
docker-compose up -d --no-build