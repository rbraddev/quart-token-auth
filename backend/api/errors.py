#!/usr/bin/env python
"""
Author: Ryan Bradshaw
Purpose: API error responses
"""

from quart import jsonify
from werkzeug.http import HTTP_STATUS_CODES


def error_response(status_code, message=None):
    """
    error response
    """

    payload = {"error": HTTP_STATUS_CODES.get(status_code, "Unknown error")}
    if message:
        payload["message"] = message
    response = jsonify(payload)
    response.status_code = status_code
    return response


def bad_request(message):
    """
    general bad request response
    """

    return error_response(400, message)
