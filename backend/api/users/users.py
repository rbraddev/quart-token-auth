"""
Author: Ryan Bradshaw
Purpose: Users routes for the quart-token-auth api
"""

import sys
from time import time
from bson.objectid import ObjectId
import jwt

from quart import jsonify, request, url_for, current_app
from api.errors import error_response, bad_request
from api.users import bp
from api.tools import send_email
from api.auth.authentication import requires_auth
from nosql.db import async_db_conn
from nosql.user import User

@bp.route("/", methods=["GET"])
@requires_auth
async def get_users(user_details=None):
    """
    Returns the whole list of users from the database
    Requires admin privileges, access level 4
    """

    if user_details["access_level"] != 4:
        return await error_response(401, "Unauthorized access")

    instance = await async_db_conn()
    userdoc = instance.register(User)

    users = [user.dump() async for user in userdoc.find({}, {"password": 0})]

    return jsonify(users)


@bp.route("/<user_id>", methods=["GET"])
@requires_auth
async def get_user(_, user_id):
    """
    Returns a Dict of the user record
    """

    instance = await async_db_conn()
    userdoc = instance.register(User)
    user = await userdoc.find_one({"id": ObjectId(user_id)}, {"password": 0})
    if not user:
        return bad_request("User does not exist")

    user.url = url_for("users.get_user", id=user.id)

    return jsonify(user.dump())


@bp.route("/create", methods=["POST"])
async def create_user():
    """
    Creates a new user and sends an email containing a confirmation token
    """

    data = await request.get_json(force=True)
    print(data, file=sys.stdout)
    firstname = data.get("firstname", None)
    lastname = data.get("lastname", None)
    email = data.get("email", None)
    password = data.get("password", None)

    try:
        instance = await async_db_conn()
        userdoc = instance.register(User)

        if None in [firstname, lastname, email, password]:
            return bad_request("user details missing")
        if await userdoc.find_one({"email": email}):
            return bad_request("An account with this email already exists")

        user = userdoc(
            firstname=firstname,
            lastname=lastname,
            email=email,
            password=password
        )
        user.set_password()
        await user.commit()

        token, _ = user.create_token(current_app.config["SECRET_KEY"])
        subject = "Account Confirmation"
        message = f"""
            <html>
                <head></head>
                <body>
                    <p>To confirm your account please 
                        <a href='https://{current_app.config["APP_URL"]}/login?confirm_token={token}'>
                            click here
                        </a>
                    </p>
                </body>
            </html>
        """
        status = await send_email(user.email, message, subject)
        if not status:
            return error_response(500, "An internal error occured")

        user = user.dump()
        user.pop("password")

        response = jsonify(user)
        response.status_code = 201
        response.headers["Location"] = url_for("users.get_user", user_id=user["id"])

        return response
    except Exception as e:
        return error_response(500, f"An internal error occured: {e}")


@bp.route("/confirm_user", methods=["PATCH"])
async def confirm_user():
    """
    confirms the users email address is valid and activates the account
    """

    confirm_token = request.args.get("confirm_token")
    if not confirm_token:
        return error_response(401, "Confirmation token required")

    try:
        api_timeout = current_app.config["API_TIMEOUT"]
        decoded_token = jwt.decode(
            confirm_token.encode(),
            current_app.config["SECRET_KEY"],
            algorithms=["HS256"],
        )
        if decoded_token["exp"] - time() >= api_timeout:
            return error_response(401, "Reset token timed out")
        email = decoded_token["email"]
    except Exception:
        return error_response(401, "Invalid confirmation token")

    try:
        instance = await async_db_conn()
        userdoc = instance.register(User)

        user = await userdoc.find_one({"email": email})
        user.confirmed = True
        await user.commit()

        user = user.dump()
        user.pop("password")

        response = jsonify(user)
        response.status_code = 201
        response.headers["Location"] = url_for("users.get_user", user_id=user["id"])

        return response
    except Exception:
        return error_response(500, "An internal error occured")
