#/usr/bin/env python
"""
Author: Ryan Bradshaw
Purpose: Authentication routes
"""

import sys
from time import time
from functools import wraps
from quart import request, current_app
import jwt
from jwt.exceptions import InvalidSignatureError, ExpiredSignatureError
from api.errors import error_response
from nosql.db import async_db_conn
from nosql.logs import Log


async def check_token(token):
    """
    requires auth wrapper checing token validity
    """

    try:
        api_timeout = current_app.config["API_TIMEOUT"]
        refresh_token = None
        expiry = None
        decoded_token = jwt.decode(
            token.encode(), current_app.config["SECRET_KEY"], algorithms=["HS256"]
        )
        user_details = {
            "email": decoded_token["email"],
            "access_level": decoded_token["access_level"],
        }

        if decoded_token["exp"] - time() <= api_timeout - 280:
            expiry = time() + api_timeout
            refresh_token = jwt.encode(
                {
                    "user_id": decoded_token["user_id"],
                    "email": decoded_token["email"],
                    "role": decoded_token["role"],
                    "access_level": decoded_token["access_level"],
                    "exp": expiry,
                },
                current_app.config["SECRET_KEY"],
                algorithm="HS256",
            ).decode("utf-8")

        return True, refresh_token, user_details, expiry

    except (InvalidSignatureError, ExpiredSignatureError):
        return False, None, None, None


def requires_auth(f):
    """
    function to authenticate routes decorated with @requires_auth
    """

    @wraps(f)
    async def decorated(*args, **kwargs):
        token = request.headers.get("Authorization")
        ip = request.remote_addr

        instance = await async_db_conn()
        logdoc = instance.register(Log)

        if not token:           
            log = logdoc(activity="invalid token", ip=ip, level="error")
            await log.commit()
            return error_response(401, "Token required")

        # token = Bearer xxxxxxxxxxxx
        # token = token.split(" ")[1]

        valid_token, refresh_token, user_details, expiry = await check_token(token)

        if not token or not valid_token:
            log = logdoc(activity="invalid token", ip=ip, level="error")
            await log.commit()
            return error_response(401, "Not a valid token")

        result = await f(user_details, *args, **kwargs)

        if refresh_token:
            log = logdoc(
                activity="token refresh",
                ip=ip,
                level="info",
                email=user_details["email"],
            )
            await log.commit()
            result.headers["refresh-token"] = refresh_token
            result.headers["token-expiry"] = expiry

        return result

    return decorated
