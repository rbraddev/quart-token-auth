"""
Author: Ryan Bradshaw
Purpose: Authentication routes
"""

from base64 import b64decode
from time import time
import jwt
from quart import jsonify, request, current_app
from api.auth import bp
from api.auth.authentication import requires_auth
from api.errors import error_response
from api.tools import send_email
from nosql.db import async_db_conn
from nosql.user import User
from nosql.logs import Log


@bp.route("/token", methods=["GET"])
async def get_token():
    """
    generates token for user authenticated routes
    """

    credentials = request.headers.get("Authorization")
    ip = request.remote_addr

    instance = await async_db_conn()
    logdoc = instance.register(Log)

    if not credentials:
        log = logdoc(ip=ip, level="error", activity="Invalid authorization header")
        await log.commit()
        return error_response(401, "Invalid authorization header")

    try:
        email, password = b64decode(credentials.split(" ")[1]).decode().split(":", 1)
    except:
        log = logdoc(ip=ip, level="error", activity="Invalid authorization header")
        await log.commit()
        return error_response(401, "Invalid authorization header")

    if None in [email, password]:
        log = logdoc(ip=ip, level="error", activity="Invalid email/password")
        await log.commit()
        return error_response(401, "Invalid email/password")

    userdoc = instance.register(User)
    user = await userdoc.find_one({"email": email})

    if not user or not user.check_password(password):
        log = logdoc(
            ip=ip, level="error", user=email, activity="Invalid username/password"
        )
        await log.commit()
        return error_response(401, "Invalid username/password")

    if not user.confirmed:
        return error_response(401, "Account has not yet need activated")

    token, expiry = user.create_token(current_app.config["SECRET_KEY"])

    log = logdoc(ip=ip, level="info", user=email, activity="User login")
    await log.commit()

    user.set_last_login()
    await user.commit()

    response = jsonify({"user": user.email, "access_level": user.access_level})
    response.headers["access-token"] = token
    response.headers["token-expiry"] = expiry

    return response


@bp.route("/check_token", methods=["GET"])
@requires_auth
async def check_token(user_details):
    """
    return user details
    """

    return jsonify(user_details)


@bp.route("/reset_password", methods=["GET", "PATCH"])
async def reset_password():
    """
    GET -> sends email to user with reset password token
    PATCH -> gets reset token and password, then changes user password
    """

    if request.method == "GET":
        email = request.args.get("email")

    instance = await async_db_conn()
    userdoc = instance.register(User)

    if request.method == "PATCH":
        data = await request.get_json(force=True)
        reset_token = request.args.get("reset_token")
        password = data.get("password")
        api_timeout = current_app.config["API_TIMEOUT"]
        try:
            decoded_token = jwt.decode(
                reset_token.encode(),
                current_app.config["SECRET_KEY"],
                algorithms=["HS256"],
            )
            if decoded_token["exp"] - time() >= api_timeout:
                return error_response(401, "Reset token timed out")
            if password is None:
                return error_response(401, "A password is required")
            email = decoded_token["email"]
        except:
            return error_response(401, "Invalid email or reset token")

    user = await userdoc.find_one({"email": email})

    if user:
        if request.method == "GET":
            if not user.confirmed:
                return error_response(401, "Account has not yet activated")

            token, _ = user.create_token(current_app.config["SECRET_KEY"])
            subject = "Password Reset"
            message = f"""
                <html>
                    <head></head>
                    <body>
                        <p>To reset your password please 
                            <a href='https://{current_app.config["APP_URL"]}/login?reset_token={token}'>
                                click here
                            </a>
                        </p>
                    </body>
                </html>
            """
            status = await send_email(user.email, message, subject)

            if status:
                response = jsonify(f"Password reset email hase been send to: {email}")
                response.status_code = 201
                return response
            else:
                return error_response(500, "An internal error occured")

        if request.method == "PATCH":
            try:
                user.password = password
                user.set_password()
                await user.commit()
                response = jsonify("Password has been reset")
                response.status_code = 201
                return response
            except:
                return error_response(500, "An internal error occured")
    else:
        return error_response(401, "Account does not exist")
