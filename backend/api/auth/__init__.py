from quart import Blueprint

bp = Blueprint("api", __name__)

from api.auth import tokens
