#!/usr/bin/env python
"""
Author: Ryan Bradshaw
Purpose: API tools
"""

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from quart import current_app


async def send_email(recipient, message, subject):
    """
    Send email using mail relay docker container
    """

    try:
        msg = MIMEMultipart()
        msg["From"] = current_app.config["APP_EMAIL"]
        msg["To"] = recipient
        msg["Subject"] = subject
        msg.attach(MIMEText(message, "html"))
        svr = smtplib.SMTP(current_app.config["MAIL_RELAY"])
        svr.sendmail(msg["From"], msg["To"], msg.as_string())
        svr.quit()
    except Exception:
        return False

    return True
