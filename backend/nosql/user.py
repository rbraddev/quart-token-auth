import sys

sys.path.append("..")
import jwt
from datetime import datetime
from time import time
from umongo import Document, fields, validate
from werkzeug.security import generate_password_hash, check_password_hash
from configuration import APIConfig


class User(Document):
    firstname = fields.StrField(required=True)
    lastname = fields.StrField(required=True)
    email = fields.StrField(required=True)
    password = fields.StrField(required=True)
    role = fields.StrField(default='user', validate=validate.OneOf(['user', 'technician', 'engineer', 'administrator']))
    access_level = fields.IntField(default=1, validate=validate.Range(min=1, max=4))
    last_login = fields.DateTimeField(default=None)
    confirmed = fields.BoolField(default=False)

    def __repr__(self):
        return f"Name: {self.firstname} {self.lastname} Email: {self.email}"

    def set_password(self):
        self.password = generate_password_hash(self.password)

    def set_access_level(self):
        access_levels = {
            "user": 1,
            "technician": 2,
            "engineer": 3,
            "administrator": 4
        }

        self.access_level = access_levels.get(self.role, 1)
    
    def set_last_login(self):
        self.last_login = datetime.now()

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def check_username(self):
        return f"{str(self.id)} is my id"

    def create_token(self, secret_key):
        expiry = time() + APIConfig.API_TIMEOUT
        token = jwt.encode(
            {
                "user_id": str(self.id),
                "email": self.email,
                "role": self.role,
                "access_level": self.access_level,
                "exp": expiry
            },
            secret_key,
            algorithm="HS256",
        ).decode("utf-8")

        return token, expiry

