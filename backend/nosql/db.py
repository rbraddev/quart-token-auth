import os
from umongo import Instance
from configuration import Config


def get_db():
    if Config.MONGODB:
        return Config.MONGODB
    else:
        raise RuntimeError("Cannot get database")


async def async_db_conn(mdb=None, port=None):
    from motor.motor_asyncio import AsyncIOMotorClient

    if port:
        mdbport = port
    else:
        mdbport = 27017

    if mdb:
        db = AsyncIOMotorClient(Config.MONGOSERVER, mdbport)[mdb]
    else:
        mdb = get_db()
        db = AsyncIOMotorClient(Config.MONGOSERVER, mdbport)[mdb]

    return Instance(db)


def sync_db_conn(mdb=None, port=None):
    from pymongo import MongoClient

    if port:
        mdbport = int(port)
    else:
        mdbport = 27017

    if mdb:
        db = MongoClient(Config.MONGOSERVER, mdbport)[mdb]
    else:
        mdb = get_db()
        db = MongoClient(Config.MONGOSERVER, mdbport)[mdb]

    return Instance(db)
