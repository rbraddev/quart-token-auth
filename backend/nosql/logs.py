from datetime import datetime
from umongo import Document, fields


class Log(Document):
    time = fields.DateTimeField(default=datetime.now())
    ip = fields.StrField()
    level = fields.StrField(required=True)
    activity = fields.StrField(required=True)
    user = fields.StrField(default=None)
    url = fields.StrField()


class InvLog(Document):
    time = fields.DateTimeField(default=datetime.now())
    nodeid = fields.IntField()
    catagory = fields.StrField()
    action = fields.StrField()
    message = fields.StrField()
