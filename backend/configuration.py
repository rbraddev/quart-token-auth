import os
from getpass import getpass


class Config():
    MONGOSERVER = os.environ.get("MONGOSERVER") or "localhost"
    MONGODB = os.environ.get("MONGODB") or "quart-token-auth"
    ADMIN_FIRSTNAME = os.environ.get("ADMIN_FIRSTNAME") or "Steve"
    ADMIN_LASTNAME = os.environ.get("ADMIN_LASTNAME") or "Austin"
    ADMIN_EMAIL = os.environ.get("ADMIN_EMAIL") or "ryan@rbradshaw.dev"
    ADMIN_PASSWORD = os.environ.get("ADMIN_PASSWORD") or "testpass123"
    # APIUSER = os.environ.get("APIUSER")
    # APIPASS = os.environ.get("APIPASS")


class APIConfig(Config):
    QUART_CORS_EXPOSE_HEADERS = {"access-token", "refresh-token", "token-expiry"}
    QUART_CORS_ALLOW_ORIGIN = {"*"}
    QUART_CORS_ALLOW_HEADERS = {"*"}
    SECRET_KEY = os.environ.get("SECRET_KEY") or "super-S3cret-kKkKk3y"
    API_TIMEOUT = 900
    MAIL_RELAY = os.environ.get("MAIL_RELAY") or "localhost"
    APP_EMAIL = os.environ.get("APP_EMAIL") or "nobody@nowhere.com"
    APP_URL = os.environ.get("APP_URL") or "localhost"
