import sys
from quart import Quart
from quart_cors import cors

from configuration import APIConfig
from nosql.db import sync_db_conn
from nosql.user import User

# check for admin user and add one if not present
instance = sync_db_conn()
user = instance.register(User)
admin = user.find_one({'role': 'administrator'})
if not admin:
    admin = user(
        firstname=APIConfig.ADMIN_FIRSTNAME,
        lastname=APIConfig.ADMIN_LASTNAME,
        email=APIConfig.ADMIN_EMAIL,
        password=APIConfig.ADMIN_PASSWORD,
        role="administrator",
        access_level=4,
        confirmed=True
    )
    admin.set_password()
    admin.commit()

app = Quart(__name__)
app = cors(app)
app.config.from_object(APIConfig)

from api.auth import bp as auth_bp
app.register_blueprint(auth_bp, url_prefix="/api/auth")

from api.users import bp as users_bp
app.register_blueprint(users_bp, url_prefix="/api/users")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000, debug=True)
