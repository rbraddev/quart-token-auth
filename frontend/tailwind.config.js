module.exports = {
  theme: {
    inset:{
      '-1': '-1rem',
    },
    extend: {
      colors: {
        'cu-blue': '#0066cc',
      }
    }
  },
  variants: {
    margin: ['active', 'focus'],
  },
  plugins: []
}
