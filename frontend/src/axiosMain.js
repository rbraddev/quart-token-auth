import axios from 'axios';
import store from './store'

export default () => {
  axios.defaults.baseURL = process.env.VUE_APP_API_URL || 'http://localhost:8000/api/'
  // axios.defaults.headers['Authorization'] = store.getters['auth/token']
  
  axios.interceptors.request.use(config => {
    // console.log('Request Interceptor', 'token:', axios.defaults.headers['Authorization'], 'end of interceptor')
    // axios.defaults.headers['Authorization'] = store.getters['auth/token']
    return config
  })
  
  axios.interceptors.response.use(
    response => {
      // console.log('Response Interceptor', response, 'end of interceptor')
      // const expirydate = new Date(response.headers['token-expiry'])
      // console.log('expiry time:', expirydate)
      // if (response.headers['access-token']) {
      //   axios.defaults.headers['Authorization'] = response.headers['access-token']
      //   console.log('default headers', axios.defaults.headers['Authorization'])
      // }
      if (response.headers['refresh-token']) {
        axios.defaults.headers['Authorization'] = response.headers['refresh-token']
        store.dispatch('auth/tokenRefresh', {
          token: response.headers['refresh-token'],
          tokenExpiry: response.headers['expiry']
        })
      }
      return response
      
    // }, error => {
    //   console.log('from interceptor', error)
    //   return error
    })
}