import BlankLayout from '@/components/layout/BlankLayout.vue'

export default {
    name: 'BlankLayout',
    created() {
        this.$parent.$emit('update:layout', BlankLayout);
    },
    render() {
        return this.$slots.default[0];
    },
}