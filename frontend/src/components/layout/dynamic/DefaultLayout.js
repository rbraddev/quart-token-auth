import DefaultLayout from '@/components/layout/DefaultLayout.vue';

export default {
    name: 'DefaultLayout',
    created() {
        this.$parent.$emit('update:layout', DefaultLayout);
    },
    render() {
        return this.$slots.default[0];
    },
};