import store from '@/store'

export function isValidToken (){
    const now = new Date().getTime()
    let expiry = new Date(store.getters['auth/tokenExpiry'])
    // const expiry = new Date(localStorage.getItem('tokenExpiry'))
    return expiry > now
}

export default {
    methods: {
        isValidToken
    }
}