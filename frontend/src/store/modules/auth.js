// import axiosAuth from '@/axiosAuth'
import axiosAuth from 'axios'

const state = {
  token: localStorage.getItem('token') || null,
  tokenExpiry: localStorage.getItem('tokenExpiry') || null,
  username: null,
  accessLvl: null
}

const getters = {
  token: state => state.token,
  tokenExpiry: state => state.tokenExpiry
}

const actions = {
  login({ commit }, authData) {
    // console.log('inside store/auth/login')
    return new Promise((resolve, reject) => {
      // console.log('inside promise')
      axiosAuth.get('auth/token', {
        auth: {
          username: authData.username,
          password: authData.password
        }
      })
        .then(res => {
          // console.log(res)
          const tokenExpiry = new Date(res.headers['token-expiry'] * 1000)
          localStorage.setItem('token', res.headers['access-token'])
          localStorage.setItem('tokenExpiry', tokenExpiry)
          axiosAuth.defaults.headers['Authorization'] = res.headers['access-token']
          commit('authUser', {
            token: res.headers['access-token'],
            tokenExpiry: tokenExpiry,
            username: res.data.username,
            accessLvl: res.data.access_level
          })
          resolve('success')
        })
        .catch(error => {
          // console.log(error.response)
          localStorage.removeItem('token')
          localStorage.removeItem('tokenExpiry')
          reject(error.response.data.message)
        })
    })
  },
  tokenRefresh({ commit }, refreshData) {
    const tokenExpiry = new Date(refreshData.expiry * 1000)
    localStorage.setItem('token', refreshData.token)
    localStorage.setItem('tokenExpiry', tokenExpiry)
    commit('tokenRefresh', {
      token: refreshData.token,
      tokenExpiry: tokenExpiry,
    })
  },
  logout({ commit }) {
    return new Promise((resolve) => {
      commit('logout')
      localStorage.removeItem('token')
      localStorage.removeItem('tokenExpiry')
      resolve()
    })
  }
}

const mutations = {
  authUser(state, userData) {
    state.token = userData.token
    state.tokenExpiry = userData.tokenExpiry
    state.username = userData.username
    state.accessLvl = userData.accessLvl
  },
  tokenRefresh(state, refreshData) {
    state.token = refreshData.token
    state.tokenExpiry = refreshData.tokenExpiry
  },
  logout(state) {
    state.token = null
    state.tokenExpiry = null
    state.username = null
    state.accessLvl = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}