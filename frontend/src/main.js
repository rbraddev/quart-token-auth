import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/css/tailwind.css'
import Vuelidate from 'vuelidate'
import axiosMain from './axiosMain'

Vue.use(Vuelidate)

axiosMain()

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')

