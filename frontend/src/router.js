import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import Inventory from './views/Inventory.vue'
import Login from './views/auth/Login.vue'
import { isValidToken } from './helpers/auth'

Vue.use(Router)

const notAuthenticated = (to, from, next) => {
  if (!isValidToken()) {
    next()
    return
  }
  next('/')
}

const authenticated = (to, from, next) => {
  if (isValidToken()) {
    next()
    return
  }
  next('/login')
}

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard,
      beforeEnter: authenticated
    },
    {
      path: '/inventory',
      name: 'inventory',
      component: Inventory,
      beforeEnter: authenticated
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      beforeEnter: notAuthenticated
    }
  ]
})

export default router